@extends('master')

@section('content')

		<!-- BREADCRUMB -->
		<div id="breadcrumb" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<h3 class="breadcrumb-header">Your cart</h3>
						<ul class="breadcrumb-tree">
							<li><a href="#">Home</a></li>
							<li class="active">Blank</li>
						</ul>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /BREADCRUMB -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
				<div id="viewCart">
				<table class="table-bordered">
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Price</th>
						<th>Quanty</th>
						<th>Total</th>
                        <th>Option</th>
                    </tr>
					@if(Session::has("Cart") !=null )
					@foreach(Session::get('Cart')->products as $item)
                    <tr>
                        <td>
							<img src="./img/{{$item['productInfo']->image}}" height="400px"  alt="">
                        </td>
                        <td>
							<h3 class="product-name"><a href="#">{{$item['productInfo']->name}}</a></h3>
                        </td>
                        <td>
						<h4 class="product-price"><span class="qty">{{$item['price']}}</span></h4>
                        </td>
						<td>
							<input id="qtyItem-{{$item['productInfo']->id}}" type="number" value="{{$item['Qty']}}">
						</td>
						<td>
							<h4 class="product-price"><span class="qty">{{$item['price']}}</h4>
						</td>
                        <td>
                            <a onclick="saveItemViewCart({{$item['productInfo']->id}});" >Save</a>
                            <span>|</span>
                            <a onclick="deleteItemViewCart({{$item['productInfo']->id}});">Delete</a>
                        </td>
                    </tr>
                    @endforeach
					@endif
                </table>
				</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- NEWSLETTER -->
		<div id="newsletter" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="newsletter">
							<p>Sign Up for the <strong>NEWSLETTER</strong></p>
							<form>
								<input class="input" type="email" placeholder="Enter Your Email">
								<button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
							</form>
							<ul class="newsletter-follow">
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-pinterest"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /NEWSLETTER -->

<script>
	function deleteItemViewCart(id) {
		$.ajax({
			url: 'Delete-Item-View-Cart/'+id,
			type: 'GET',
		}).done(function(response){
			RenderViewCart(response)
			alertify.success('Deleted');
		});
	}

	function saveItemViewCart(id) {
		console.log($("#qtyItem-"+id).val());
	}
	function RenderViewCart(response) {
		if (response) {
			$("#viewCart").empty();
			$("#viewCart").html(response);
		}
	}
</script>

@endsection