	@extends('master')

@section('content')
		<!-- BREADCRUMB -->
		<div id="breadcrumb" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<ul class="breadcrumb-tree">
							<li><a href="#">Home</a></li>
							<li class="active">All Products</li>
						</ul>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /BREADCRUMB -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					
					<!-- STORE -->
					<div id="store" class="">
						

						<!-- store products -->
						<div class="row">
							@foreach($products as $product)
							<!-- product -->
							<div class="col-md-3 col-xs-4">
								<div class="product">
									<a href="{{ route('detail',$product->id) }}">
                                        <div class="product-img">
                                            <img src="./img/{{ $product->image }}" alt="">
                                            <div class="product-label">
                                            	@if($product->promotion_price!= null)
                                                <span class="sale">-{{ceil(100-($product->promotion_price / $product->unit_price *100))}}%</span>
                                                @endif
                                               
                                                <span class="new">NEW</span>
                                                
                                            </div>
                                        </div>
                                        <div class="product-body">
                                            <!--<p class="product-category">{{ $product->name }}</p>-->
                                            <h3 class="product-name"><a href="#">{{ $product->name }}</a></h3>
                                            <h4 class="product-price">${{ $product->promotion_price }} <del
                                                    class="product-old-price">${{ $product->unit_price }}</del></h4>
                                        </div>    
                                    </a>
									
									<div class="add-to-cart">
                                    <button class="add-to-cart-btn"><a onclick="addCart({{$product->id}})"
                                            href="javascript:"><i class="fa fa-shopping-cart"></i> add to
                                            cart</a></button>
                                	</div>
								</div>
							</div>
							<!-- /product -->
							@endforeach
						</div>
						<!-- /store products -->

						<!-- store bottom filter -->
						<div class="store-filter clearfix">
							
							<ul class="store-pagination">
								<li class="active">1</li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
							</ul>
						</div>
						<!-- /store bottom filter -->
					</div>
					<!-- /STORE -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- NEWSLETTER -->
		<div id="newsletter" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="newsletter">
							<p>Sign Up for the <strong>NEWSLETTER</strong></p>
							<form>
								<input class="input" type="email" placeholder="Enter Your Email">
								<button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
							</form>
							<ul class="newsletter-follow">
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-pinterest"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /NEWSLETTER -->

@endsection