<div class="cart-list">
@if(Session::has("Cart") !=null  )
@foreach(Session::get('Cart')->products as $item)
<div class="product-widget">
	<div class="product-img">
		<img src="./img/{{$item['productInfo']->image}}" alt="">
	</div>
	<div class="product-body">
		<h3 class="product-name"><a href="#">{{$item['productInfo']->name}}</a></h3>
		<h4 class="product-price"><span class="qty">{{$item['Qty']}}x</span>${{$item['price']}}</h4>
	</div>
	<button class="delete" id="delete" data-id="{{$item['productInfo']->id}}"><i class="fa fa-close"></i></button>
</div>
@endforeach
	<input hidden type="number" value="{{Session::get('Cart')->totalQty}}" id="totalQtyCart">
@endif
</div>
@if(Session::has("Cart") !=null )
	<div class="cart-summary">
		<small id="totalQtyShow2">{{Session::get('Cart')->totalQty}} Item(s) selected</small>
		<h5 id="totalPriceShow">SUBTOTAL: {{Session::get('Cart')->totalPrice}}</h5>
	</div>
@else
	<div class="cart-summary">
		<small>0 Item(s) selected</small>
		<h5>SUBTOTAL: 0</h5>
	</div>
@endif
