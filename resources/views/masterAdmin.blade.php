<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>
<style type="text/css">
html,
body {
    height: 100%;
    margin: 0;
}

a {
    text-decoration: none !important;
}

ul {
    list-style: none !important;
}

table,
th,
td {
    border: 1px solid black;
}

table {
    width: 100%;
    align-content: center;
}

.container-fluid {
    margin: 0;
    padding: 0;
}

#navbar-header {
    display: flex;
    justify-content: flex-end;
    background-color: black;
    padding: 12px;
}

#navbar-header a {
    margin: 0px 20px 0px 20px;
}

#content {
    height: 100%;
}

#treeview {
    background-color: lightblue;
    height: 100%;
}

#treeview a {
    display: block;
    border: 1px solid black;
    padding: 6px;
}

.profile {
    text-align: center;
}

#content2 {
    align-content: center;
    text-align: center;
}

footer {
    background: black;
    text-align: center;
    color: white;

}
</style>
<script>
    
</script>
<body>

    <div id="navbar-header">
        <a href="{{ route('logout') }}">Logout</a>

    </div>
    <div class="container-fluid" id="content">
        <div class="col col-lg-2" id="treeview">
            <div class="profile">
                <div><img src="" alt="Ảnh"></div>
                <div>
                    <h3>Name</h3>
                </div>
            </div>
            <hr>
            <div>
                <a href="{{ url('admin')}}">Sản phẩm</a>
                <a href="{{ url('create')}}">Thêm</a>
                <a href="{{ url('quanlytaikhoan')}}">Quan ly tai khoan</a>
            </div>
        </div>
        <div class="col col-lg-10" id="content">
        @yield('content')
        </div>
    </div>
    
</body>

</html>