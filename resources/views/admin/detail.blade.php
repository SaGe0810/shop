@extends('masterAdmin')

@section('content')


<div class="col col-lg-10" id="content2">

    <h2>Edit product</h2>
    <form method="POST" action="{{ route('detail', $product->id) }}">
        <div class="form-group">
            {{ csrf_field() }}
            <label for="image">Image:</label>
            <input name="image" class="form-control" value="{{ $product->image }}">
            <label for="name">Name:</label>
            <input name="name" class="form-control" value=" {{ $product->name }} ">
            <label for="id_type">Type:</label>
            <input name="id_type" class="form-control" value=" {{ $product->id_type }} ">
            <label for="description">Description:</label>
            <input name="description" class="form-control" value=" {{ $product->description }} ">
            <label for="unit_price">Price:</label>
            <input name="unit_price" class="form-control" value=" {{ $product->unit_price }} ">
            <label for="promotion_price">Promotion Price:</label>
            <input name="promotion_price" class="form-control" value=" {{ $product->promotion_price }} ">
        </div>
        <button type="submit" name="save" class="btn btn-primary">Save</button>
    </form>
</div>
</div>
@endsection