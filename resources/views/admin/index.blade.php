@extends('masterAdmin')

@section('content')
<table>
        <tr>
            <th>id</th>
            <th>Image</th>
            <th>name</th>
            <th>Type</th>
            <th>price</th>
            <th>promotion price</th>
            <th>Option</th>
        </tr>
    @foreach($products as $product)
    <tr>
        <td>{{ $product->id }}</td>
        <td><a href=""><img src="./img/{{ $product->image }}" height="50px" alt="ảnh"></a></td>
        <td>{{ $product->getName() }}</td>
        <td>{{ $product->getType() }}</td>
        <td>{{ $product->unit_price }}</td>
        <td>{{ $product->promotion_price }}</td>
        <td>
            <a href="{{ route('detail', $product->id) }}">Edit</a>
            <span>|</span>
            <a href="{{ route('delete', $product->id) }}">Delete</a>
        </td>
    </tr>
    @endforeach
</table>
@endsection