@extends('masterAdmin')

@section('content')
        <div class="col col-lg-10" id="content2">
            <h2>Edit product</h2>
            <form method="POST" action="">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input name="name" class="form-control" value="">
                </div>
                <div class="form-group">
                    <label for="price">Price:</label>
                    <input name="price" class="form-control" value="">
                </div>  
                    <button type="submit" name="save" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
    