@extends('masterAdmin')

@section('content')
<form action="" method="post">
    {{ csrf_field() }}
    Search:
    <input type="text" name="keyword" id="search">
    <input type="submit" name="search" value="Search">
</form>
<table>
    <tr>
            <th>id</th>
            <th>Image</th>
            <th>name</th>
            <th>Type</th>
            <th>price</th>
            <th>promotion price</th>
            <th>Option</th>
        </tr>
       <div id="result">
            @foreach($products as $product)
    <tr>
        <td>{{ $product->id }}</td>
        <td><a href=""><img src="./img/{{ $product->image }}" height="50px" alt="ảnh"></a></td>
        <td>{{ $product->getName() }}</td>
        <td>{{ $product->getType() }}</td>
        <td>{{ $product->unit_price }}</td>
        <td>{{ $product->promotion_price }}</td>
        <td>
            <a href="{{ route('detail', $product->id) }}">Edit</a>
            <span>|</span>
            <a href="{{ route('delete', $product->id) }}">Delete</a>
        </td>
    </tr>
    @endforeach
       </div> 

</table>
<!--<script type="text/javascript">
    $("#result").on("click", "#search", function() {
        $.ajax({
            url: 'search',
            type: 'GET',
        }).done(function(response) {
            RenderSearch(response);

        });
    });

    function RenderSearch(response) {
        if (response) {
            $("#result").empty();
            $("#result").html(response);
        }
    }
</script> -->
@endsection