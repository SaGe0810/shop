<table class="table-bordered">
    <tr>
        <th>Image</th>
        <th>Name</th>
        <th>Price</th>
        <th>Quanty</th>
        <th>Total</th>
        <th>Option</th>
    </tr>
    @if(Session::has("Cart") !=null )
    @foreach(Session::get('Cart')->products as $item)
    <tr>
        <td>
            <img src="./img/{{$item['productInfo']->image}}" alt="">
        </td>
        <td>
            <h3 class="product-name"><a href="#">{{$item['productInfo']->name}}</a></h3>
        </td>
        <td>
            <h4 class="product-price"><span class="qty">{{$item['price']}}</span></h4>
        </td>
        <td>
            <input id="qtyItem-{{$item['productInfo']->id}}" type="number" value="{{$item['Qty']}}">
        </td>
        <td>
            <h4 class="product-price"><span class="qty">{{$item['price']}}</h4>
        </td>
        <td>
            <a onclick="saveItemViewCart({{$item['productInfo']->id}});">Save</a>
            <span>|</span>
            <a onclick="deleteItemViewCart({{$item['productInfo']->id}});">Delete</a>
        </td>
    </tr>
    @endforeach
    @endif
</table>