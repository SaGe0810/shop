<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";

    public function product()
    {
        return $this->belongsTo('App\Product', 'id_product', 'id');
    }
}
