<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";

    const TYPE = [
        1 => 'Kinh doanh',
        2 => 'Ngừng',
        3 => '...',
    ];
    /**
     * Get the product_type that owns the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_type()
    {
        return $this->belongsTo('App\ProductType', 'id_type', 'id');
    }

    /**
     * Get all of the bill_detail for the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bill_detail()
    {
        return $this->hasMany('App\BillDetail', 'id_product', 'id');
    }

    public function getName()
    {
        return $this->name; // . ' - ' . $this->unit_price;
    }

    public function getType()
    {
        return array_key_exists($this->id_type, self::TYPE) ? self::TYPE[$this->id_type] : '--';
    }
}