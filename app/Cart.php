<?php

namespace App;

class Cart
{
    public $products = null;
    public $totalPrice = 0;
    public $totalQty = 0;

    public function __construct($cart)
    {
        if ($cart) {
            $this->products = $cart->products;
            $this->totalPrice = $cart->totalPrice;
            $this->totalQty = $cart->totalQty;
        }
    }
    public function AddCart($product, $id)
    {
        $newProduct = ['Qty' => 0, 'price' => $product->unit_price, 'productInfo' => $product];
        if ($this->products) {
            if (array_key_exists($id, $this->products)) {
                $newProduct = $this->products[$id];
            }
        }
        $newProduct['Qty']++;
        $newProduct['price'] = $newProduct['Qty'] * $product->unit_price;
        $this->products[$id] = $newProduct;
        $this->totalPrice += $product->unit_price;
        $this->totalQty++;
    }

    public function DeleteItemCart($id)
    {
        $this->totalQty -= $this->products[$id]['Qty'];
        $this->totalPrice -= $this->products[$id]['price'];
        unset($this->products[$id]);
    }

    public function UpdateItemCart($id, $qty)
    {
        $this->totalQty -= $this->products[$id]['Qty'];
        $this->totalPrice -= $this->products[$id]['price'];

        $this->products[$id]['Qty'] = $qty;
        $this->products[$id]['price'] = $qty * $this->products[$id]['productInfo']->price;

        $this->totalQty += $this->products[$id]['Qty'];
        $this->totalPrice += $this->products[$id]['price'];
    }
}