<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', [
    'as' => 'home',
    'uses' => 'PageController@getIndex',
]);

Route::get('store', [
    'as' => 'store',
    'uses' => 'PageController@getStore',
]);

Route:get('detail-product/{id}', [
    'as' => 'detail-product',
    'uses' => 'PageController@getDetail',
]);

Route::get('Add-Cart/{id}', [
    'as' => 'addCart',
    'uses' => 'PageController@addCart',
]);

Route::get('Delete-Item-Cart/{id}', [
    'as' => 'deleteItemCart',
    'uses' => 'PageController@deleteItemCart',
]);

Route::get('viewCart', [
    'as' => 'viewcart',
    'uses' => 'PageController@viewCart',
]);

Route::get('Delete-Item-View-Cart/{id}', [
    'as' => 'deleteItemViewCart',
    'uses' => 'PageController@deleteItemViewCart',
]);

Route::get('Save-Item-View-Cart/{id}/{qty}', [
    'as' => 'deleteItemViewCart',
    'uses' => 'PageController@saveItemViewCart',
]);

Route::get('admin', [
    'as' => 'admin',
    'uses' => 'AdminController@getAdmin',
]);

Route::get('create', [
    'as' => 'create',
    'uses' => 'AdminController@AdminCreate',
]);

Route::post('create', [
    'as' => 'create',
    'uses' => 'AdminController@Create',
]);

Route::get('detail/{id}', [
    'as' => 'detail',
    'uses' => 'AdminController@show',
]);

Route::post('detail/{id}', [
    'as' => 'detail',
    'uses' => 'AdminController@edit',
]);

Route::get('delete/{id}', [
    'as' => 'delete',
    'uses' => 'AdminController@destroy',
]);

Route::get('auth/login', [
    'as' => 'login',
    'uses' => 'LoginController@getLogin',
]);

Route::post('auth/login', [
    'as' => 'login',
    'uses' => 'LoginController@postLogin',
]);

Route::get('auth/register', [
    'as' => 'register',
    'uses' => 'LoginController@getRegister',
]);

Route::post('auth/register', [
    'as' => 'register',
    'uses' => 'LoginController@postRegister',
]);

Route::get('logout', [
    'as' => 'logout',
    'uses' => 'LoginController@logout',
]);

Route::get('search', [
    'as' => 'search',
    'uses' => 'AdminController@getSearch',
]);

Route::post('search', [
    'as' => 'search',
    'uses' => 'AdminController@search',
]);

Route::get('quanlytaikhoan', [
    'as' => 'quanlytaikhoan',
    'uses' => 'AdminController@quanLyTaiKhoan',
]);

Route::get('checkout', [
    'as' => 'checkout',
    'uses' => 'PageController@checkOut',
]);

Route::post('checkout', [
    'as' => 'checkout',
    'uses' => 'PageController@postCheckOut',
]);