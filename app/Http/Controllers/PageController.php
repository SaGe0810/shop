<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Http\Controllers\Controller;
use App\News;
use App\Product;
use App\Customer;
use App\Bill;
use App\BillDetail;
use App\User;
use Illuminate\Http\Request;
use Session;

class PageController extends Controller
{
    public function getIndex()
    {
        $products = Product::all();
        $news = News::all();
        return view('page.index', compact('news', 'products'));
    }

    
    public function create()
    {
        //
    }

    public function getStore(Request $request)
    {
        $products = Product::all();
        return view('page.store', compact('products'));
    }

    
    public function getDetail($id)
    {
        $sanpham = Product::where('id', $id)->first();
        $sp_tuongtu = Product::where('id_type', $sanpham->id_type)->paginate(4);
        return view('page.product', compact('sanpham', 'sp_tuongtu'));
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }

    public function addCart(Request $req, $id)
    {
        $products = Product::where('id', $id)->first();
        if ($products != null) {
            $oldCart = Session('Cart') ? Session('Cart') : null;
            $newCart = new Cart($oldCart);
            $newCart->AddCart($products, $id);

            $req->session()->put('Cart', $newCart);

        }
        return view('cart');
    }

    public function deleteItemCart(Request $req, $id)
    {
        $oldCart = Session('Cart') ? Session('Cart') : null;
        $newCart = new Cart($oldCart);
        $newCart->DeleteItemCart($id);
        if (Count($newCart->products) > 0) {
            $req->Session()->put('Cart', $newCart);
        } else {
            $req->Session()->forget('Cart');
        }
        return view('cart');
    }

    public function viewCart()
    {
        return view('page.viewCart');
    }

    public function deleteItemViewCart(Request $req, $id)
    {
        $oldCart = Session('Cart') ? Session('Cart') : null;
        $newCart = new Cart($oldCart);
        $newCart->DeleteItemCart($id);
        if (Count($newCart->products) > 0) {
            $req->Session()->put('Cart', $newCart);
        } else {
            $req->Session()->forget('Cart');
        }
        return view('view-Cart');
    }

    public function saveItemViewCart(Request $req, $id, $qty)
    {
        $oldCart = Session('Cart') ? Session('Cart') : null;
        $newCart = new Cart($oldCart);
        $newCart->UpdateItemCart($id, $qty);
        $req->Session()->put('Cart', $newCart);

        return view('view-Cart');
    }

    public function checkOut(Request $req)
    {
        $oldCart = Session('Cart') ? Session('Cart') : null;
        $newCart = new Cart($oldCart);
        $req->Session()->put('Cart', $newCart);

        return view('page.checkout');
    }

    public function postCheckOut(Request $req)
    {
        $cart=Session::get('Cart');

        $customer = new Customer;
        $customer->name = $req->name;
        $customer->email = $req->email;
        $customer->address = $req->address;
        $customer->phone_number = $req->phone;
        $customer->note = $req->note;
        if ($req->password!= null) {
            $user = new User;
            $user->full_name = $customer->name;
            $user->email = $customer->email;
            $user->password = bcrypt($req->password);
            $user->save();
        }
        $customer->save();

        $bill = new Bill;
        $bill->id_customer = $customer->id;
        $bill->date_oder = date('Y-m-d');
        $bill->total = $cart->totalPrice;
        $bill->note = $req->note;
        $bill->save();

        foreach ($cart->products as $key => $value) {
            $billdetail = new BillDetail;
            $billdetail->id_bill = $bill->id;
            $billdetail->id_product = $key;
            //$billdetail->quality = $value['Qty'];
            $billdetail->unit_price = $value['price']/$value['Qty'];
            $billdetail->save();
        }
        Session::forget('Cart');

        return redirect('/');
    }
}