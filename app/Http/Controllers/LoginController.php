<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use DB;
use Auth;
use App\User;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('auth/login');
    }

    public function postLogin(Request $req)
    {
        $email = $req->email;
        $password = $req->password;

        //$checkLogin = DB::table('users')->where(['email'=>$email, 'password'=>$password])->get();
        Auth::attempt([
            'email' => $email,
            'password' => $password,
        ]);

        if (Auth::attempt([
            'email' => $email,
            'password' => $password,
        ])) {
            return redirect('admin');
        } else {
            return redirect('auth/login');
        }
    }

    public function getRegister()
    {
        return view('auth/register');
    }

    public function postRegister(Request $req)
    {
        $user = User::all();
        $user = new User;
        $user->full_name = $req->name;
        $user->email = $req->email;
        $user->password = bcrypt($req->password);
        if ($req->password == $req->password_confirmation) {
            $user->save();
            return redirect('auth/login');
        } else {
            return redirect('auth/register');
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }
}